package com.example.oneclick;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OneclickApplicationTests {

	private HelloControler controler = new HelloControler();
	@Test
	public void contextLoads() {
		Assert.assertThat(controler.sayHello(), equalTo("hello world"));
	}

}
